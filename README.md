# The CoTech Fund

## What is the CoTech Fund?

Following a [successful pilot year](https://community.coops.tech/t/cotech-fund-pilot-year-report/1948) we have decided to make the CoTech fund permanent. The fund is a voluntary system where members of CoTech can contribute £1 per member, per week. The subscription is collected annually in advance. The fund can be used for any purpose that supports CoTech including, but not limited to:

- Marketing and promotion
- Events
- Paying for infrastructure costs (such as the [community forum](https://community.coops.tech))

Those co-ops who have joined the fund decide collectively how the fund should be distributed. We try to achieve this by consent, but where necessary can use a "one co-op, one vote" rule.

## How do I join the fund?

Send an email to `treasurer@coops.tech` and tell us how many members are in your co-op. We will then invoice you for the appropriate amount.

## How do I propose spending to the fund?
According to [the process outlined in 2019](https://wiki.coops.tech/wiki/Sheffield_2019/CoTech_Fund):
- A thread will be created on [CoTech Loomio](https://www.loomio.org/cotech) to propose a spending item and the rationale behind it - [an example](https://www.loomio.org/p/du70xYHB/cotech-hubl-set-up-an-instance-on-happy-dev-server-at-a-cost-of-105-for-deployment-labour).
- Co-ops [contributing to the fund](https://git.coop/cotech/fund/-/tree/master/invoices/sent) will have a week to inspect the notion and block it if they disagree. If there are no objections, the spending is approved. 


## Where can I find out more about the fund?

There is a [fund category](https://community.coops.tech/c/cotech/fund/25) on the community forum where we post fund updates and discuss how to use the fund. Everyone can read these updates, and members of the co-ops in the fund can post.

## How do I invoice the fund?

```
Co-operative Technologists
Space4
149 Fonthill Road
London N4 3HF
United Kingdom
```

