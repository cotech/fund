

![](../../../img/Cotech-blue-text.png)





<pre>
Co-operative Web Ltd.
2 Devon Way
Longbridge
Birmingham
B31 2TS
</pre>


12th April 2021



# Invoice: CoTech Fund Contribution
## Invoice number: 00029



| Description                                                  | Members | Total   |
| ------------------------------------------------------------ | ------- | ------- |
| Quaterly subscription to CoTech fund @ £1/member/week (Q4 2020/2021) | 17      | £221.00 |

Please make a payment of £221.00 by bank transfer to:

- **Account Name**: Co-operative Technologists
- **Account Number**: 20409157
- **Sort Code**: 60-83-01



Kind regards,

![](../../../img/chris_lowis_signature.png)



(Chris Lowis, Treasurer)

<span  class="footer"> Chris Lowis | treasurer@coops.tech</span>
