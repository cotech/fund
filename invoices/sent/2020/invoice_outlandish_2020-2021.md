

![](../../../img/Cotech-blue-text.png)





<pre>
Outlandish
Tower House
Unit 10
139-149 Fonthill Road
London
N4 3HF
</pre>


19th June 2020



# Invoice: CoTech Fund Contribution
## Invoice number: 00014



| Description                                         | Members | Total   |
| --------------------------------------------------- | ------- | ------- |
| Annual subscription to CoTech fund @ £1/member/week | 8       | £416.00 |

Please make a payment of £416.00 by bank transfer to:

- **Account Name**: Co-operative Technologists
- **Account Number**: 20409157
- **Sort Code**: 60-83-01



Kind regards,

![](../../../img/chris_lowis_signature.png)



(Chris Lowis, Treasurer)

<span  class="footer"> Chris Lowis | treasurer@coops.tech</span>
