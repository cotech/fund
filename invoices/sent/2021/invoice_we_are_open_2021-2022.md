

![](../../../img/Cotech-blue-text.png)





<pre>
We Are Open
3 South Terrace
Morpeth
Northumberland
NE61 1DZ
</pre>




30th April 2021



# Invoice: CoTech Fund Contribution
## Invoice number: 00044



| Description                                         | Members | Total   |
| --------------------------------------------------- | ------- | ------- |
| Annual subscription to CoTech fund @ £1/member/week | 4       | £208.00 |

Please make a payment of £208.00 by bank transfer to:

- **Account Name**: Co-operative Technologists
- **Account Number**: 20409157
- **Sort Code**: 60-83-01



Kind regards,

![](../../../img/chris_lowis_signature.png)



(Chris Lowis, Treasurer)

<span  class="footer"> Chris Lowis | treasurer@coops.tech</span>