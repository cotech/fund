

![](../../../img/Cotech-blue-text.png)





<pre>
InFact Co-operative
c/o SPACE4
113-115 Fonthill Road
London
N4 3HH
</pre>





30th April 2021



# Invoice: CoTech Fund Contribution
## Invoice number: 00039



| Description                                         | Members | Total   |
|-----------------------------------------------------|---------|---------|
| Annual subscription to CoTech fund @ £1/member/week | 3       | £156.00 |

Please make a payment of £156.00 by bank transfer to:

- **Account Name**: Co-operative Technologists
- **Account Number**: 20409157
- **Sort Code**: 60-83-01



Kind regards,

![](../../../img/chris_lowis_signature.png)



(Chris Lowis, Treasurer)

<span  class="footer"> Chris Lowis | treasurer@coops.tech</span>