

![](../../../img/Cotech-blue-text.png)





<pre>
Co-operative Web Ltd.
2 Devon Way
Longbridge
Birmingham
B31 2TS
</pre>


24th March 2022



# Invoice: CoTech Fund Contribution
## Invoice number: 00049



| Description                                                  | Members | Total   |
| ------------------------------------------------------------ | ------- | ------- |
| Quaterly subscription to CoTech fund @ £1/member/week (Q4 2021/2022) | 16      | £208.00 |

Please make a payment of £208.00 by bank transfer to:

- **Account Name**: Co-operative Technologists
- **Account Number**: 20409157
- **Sort Code**: 60-83-01



Kind regards,

![](../../../img/chris_lowis_signature.png)



(Chris Lowis, Treasurer)

<span  class="footer"> Chris Lowis | treasurer@coops.tech</span>
