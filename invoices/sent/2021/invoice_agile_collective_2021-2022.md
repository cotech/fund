

![](../../../img/Cotech-blue-text.png)





<pre>
Agile Collective Ltd
The Old Music Hall
106 - 108 Cowley Road
Oxford
England
OX4 1JE
</pre>




30th April 2021



# Invoice: CoTech Fund Contribution
## Invoice number: 00030



| Description                                         | Members | Total   |
| --------------------------------------------------- | ------- | ------- |
| Annual subscription to CoTech fund @ £1/member/week | 13      | £676.00 |

Please make a payment of £676.00 by bank transfer to:

- **Account Name**: Co-operative Technologists
- **Account Number**: 20409157
- **Sort Code**: 60-83-01



Kind regards,

![](../../../img/chris_lowis_signature.png)



(Chris Lowis, Treasurer)

<span  class="footer"> Chris Lowis | treasurer@coops.tech</span>