

![](../../../img/Cotech-blue-text.png)





<pre>
Principle Six LLP
198 Old Ford Road
London
E2 9PT
</pre>




30th April 2021



# Invoice: CoTech Fund Contribution
## Invoice number: 00041



| Description                                         | Members | Total   |
| --------------------------------------------------- | ------- | ------- |
| Annual subscription to CoTech fund @ £1/member/week | 2       | £104.00 |

Please make a payment of £104.00 by bank transfer to:

- **Account Name**: Co-operative Technologists
- **Account Number**: 20409157
- **Sort Code**: 60-83-01



Kind regards,

![](../../../img/chris_lowis_signature.png)



(Chris Lowis, Treasurer)

<span  class="footer"> Chris Lowis | treasurer@coops.tech</span>