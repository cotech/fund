require 'csv'
require 'erb'

FIRST_INVOICE_NUMBER = 50
INVOICE_DATE = '20 May 2022'

output_dir = File.join(File.dirname(__FILE__), "..", "invoices", "sent", "2022")

members_fn = File.read(File.join(File.dirname(__FILE__), "..", "members.csv"))
members = CSV.parse(members_fn, headers: true)

template_fn = File.read(File.join(File.dirname(__FILE__), "..", "template", "template.html"))

class Invoice
  def initialize(data, number)
    @coop_name = data['coop_name']
    @contact_address = data['contact_address']
    @members = data['members']
    @number = number
  end

  def date
    INVOICE_DATE
  end

  def number
    "%04d" % @number
  end

  def members
    @members.to_i
  end

  def total
    members * 52
  end

  def name
    @coop_name
  end

  def basename
    name.downcase.gsub(' ', '_').gsub('.', '')
  end

  def address
    @contact_address.gsub("//", "\n")
  end

  def get_binding
    binding()
  end
end

members.each_with_index do |member, index|
  invoice = Invoice.new(member, FIRST_INVOICE_NUMBER + index)
  renderer = ERB.new(template_fn)
  result = renderer.result(invoice.get_binding)

  output_html_fn = File.join(output_dir, invoice.basename + '.html')
  output_pdf_fn = File.join(output_dir, invoice.basename + '.pdf')

  next if File.exist?(output_html_fn)

  File.write(output_html_fn, result)

  system("wkhtmltopdf #{output_html_fn} #{output_pdf_fn}")
end
