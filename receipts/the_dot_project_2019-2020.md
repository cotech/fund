---
papersize: a4
margin-left: 20mm
margin-right: 25mm
margin-top: 10mm
margin-bottom: 20mm
...

![](img/Cotech-blue-text.png){ width=30mm }

<pre>
The Dot Project
2 Beech View
Bath
England
BA2 6DX
</pre>

6th August 2019

# Receipt: CoTech Fund Contribution
## Receipt number: 00005

Dear Annie,

Thank you for The Dot Project's subscription to the CoTech fund for the period May 2019 to May 2020. On the 1st July 2019 we received your contribution of

**£156.00**

Kind regards,

![](img/chris_lowis_signature.png){ width=33.8mm }

(Chris Lowis, Treasurer)

Chris Lowis | treasurer@coops.tech
