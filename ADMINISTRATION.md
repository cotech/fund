# Administration

Records and documentation for the CoTech fund

## Generating receipts

Install `wkhtmltopdf` using, for example homebrew on OS X

```
brew install Caskroom/cask/wkhtmltopdf
```

Then in the root of the project run

```
ruby scripts/generate_invoices.rb
```

The invoices should be generated in `invoices/sent/2022/`.

The ruby script can be edited to change the start of the range of invoice numbers and to generate the output in a different directory.

The data used to generate the invoices is in `members.csv`.

## Running reports

We keep a plain-text ledger of transactions in ~cotech.journal~. Using the [hledger](https://hledger.org/) tool we can generate reports. A typical one is the (financial) year-by-year break down of income and expenses which we report occasionally on the [discourse forum](https://community.coops.tech/c/cotech/fund/25).

```
hledger -f cotech.journal is -p "yearly from 2019-04-06"
```
